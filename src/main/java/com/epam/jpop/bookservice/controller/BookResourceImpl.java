package com.epam.jpop.bookservice.controller;

import com.epam.jpop.bookservice.model.Book;
import com.epam.jpop.bookservice.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.inject.Inject;
import javax.inject.Named;
import java.net.URI;
import java.util.List;

@Named
public class BookResourceImpl implements BookResource {

    @Inject
    private BookService bookService;

    @Override
    public ResponseEntity<List<Book>> getAllBooks() {
        return ResponseEntity.ok().body(bookService.getAllBooks());
    }

    @Override
    public ResponseEntity<Book> getBook(@PathVariable("id") Long id) {
        return ResponseEntity.ok().body(bookService.getBook(id));
    }

    @Override
    public ResponseEntity<Book> saveBook(@RequestBody Book book) {
        Book savedBook = bookService.saveBook(book);

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(savedBook.getId()).toUri();

        return ResponseEntity.created(location).body(savedBook);
    }

    @Override
    public ResponseEntity<Object> deleteBook(@PathVariable("id") Long id) {
        bookService.deleteBook(id);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Object> updateBook(@RequestBody Book book, @PathVariable("id") Long id) {
        bookService.updateBook(book, id);
        return ResponseEntity.ok().build();
    }
}
