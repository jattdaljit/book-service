package com.epam.jpop.bookservice.controller;

import com.epam.jpop.bookservice.model.Book;
import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(value = "Book Resource", description = "Contains all operations concerning book")
@RequestMapping(value = "/book")
public interface BookResource {

    @ApiOperation(value = "View a list of available book", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list")
    })

    @GetMapping
    ResponseEntity<List<Book>> getAllBooks();

    @ApiOperation(value = "View a single book", response = Book.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved book"),
            @ApiResponse(code = 404, message = "Book not found with id")
    })

    @GetMapping(value = "{id}")
    ResponseEntity<Book> getBook(
            @ApiParam(value = "Book ID from which the book will be retrieved", required = true) @PathVariable("id") Long id);

    @ApiOperation(value = "Save a book", response = Book.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully saved a book")
    })

    @PostMapping
    ResponseEntity<Book> saveBook(
            @ApiParam(value = "Book object", required = true) @RequestBody Book book);

    @ApiOperation(value = "Delete a book", response = ResponseEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully deleted book"),
            @ApiResponse(code = 404, message = "Book not found with id")
    })

    @DeleteMapping(value = "{id}")
    ResponseEntity<Object> deleteBook(
            @ApiParam(value = "Book ID based on which the book will be deleted", required = true) @PathVariable("id") Long id);

    @ApiOperation(value = "Update single book", response = ResponseEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated book"),
            @ApiResponse(code = 404, message = "Book not found with id")
    })

    @PutMapping(value = "{id}")
    ResponseEntity<Object> updateBook(
            @ApiParam(value = "Book object", required = true) @RequestBody Book book,
            @ApiParam(value = "Book ID based on which the book will be updated", required = true) @PathVariable("id") Long id);
}
