package com.epam.jpop.bookservice.service;

import com.epam.jpop.bookservice.exception.BookNotFoundException;
import com.epam.jpop.bookservice.model.Book;
import com.epam.jpop.bookservice.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

@Service
public class BookService {

    @Inject
    private BookRepository bookRepository;

    public List<Book> getAllBooks() {
        return bookRepository.findAll();
    }

    public Book getBook(Long id) {
        Optional<Book> searchedBook = bookRepository.findById(id);
        searchedBook.orElseThrow(() -> new BookNotFoundException());

        return searchedBook.get();
    }

    public Book saveBook(Book book) {
        Book savedBook = bookRepository.save(book);
        return savedBook;

    }

    public void deleteBook(Long id) {
        isBookExist(id);
        bookRepository.deleteById(id);
    }

    public void updateBook(Book book, Long id) {
        isBookExist(id);
        book.setId(id);
        bookRepository.save(book);
    }

    private void isBookExist(Long id) {
        Optional<Book> searchedBook = bookRepository.findById(id);
        searchedBook.orElseThrow(() -> new BookNotFoundException());
    }
}
